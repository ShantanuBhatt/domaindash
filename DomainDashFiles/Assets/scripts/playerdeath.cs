﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerdeath : MonoBehaviour
{
    public GameObject scenemanger;
    public movement player;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    void OnCollisionEnter(Collision collisioninfo)
    {
        if (collisioninfo.collider.tag == "enemy")
        {
            scenemanger.GetComponent<scenemanager>().EndGame();
            player.enabled = false;

        }
    }
}
